<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Mahsa\VoteComment\Models\VoteComment;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function voteComments()
    {
        return $this->hasMany(VoteComment::class, 'user_id');
    }

    public function ownedProducts()
    {
        return $this->hasMany(Product::class, 'owner_id');
    }

    public function purchasedProducts()
    {
        return $this->belongsToMany(Product::class, 'factors', 'user_id', 'product_id')
            ->withPivot('status')->withTimestamps();
    }

    public function checkUserPurchased(Product $product)
    {
        return $this->purchasedProducts()->where('product_id', $product->id)->where('status','paid')->exists();
    }
}
