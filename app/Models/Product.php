<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mahsa\VoteComment\Interfaces\VoteAndCommentableInterface;
use Mahsa\VoteComment\Traits\VoteAndCommentableTrait;

class Product extends Model implements VoteAndCommentableInterface
{
    use HasFactory;
    use VoteAndCommentableTrait;

    public $timestamps = true;

    public function getVoteAverageAttribute()
    {
        return $this->getVoteAverage();
    }

    public function getVoteCountAttribute()
    {
        return $this->getVoteCount(true);
    }

    public function getCommentCountAttribute()
    {
        return $this->getCommentCount(true);
    }

    public function getRecentCommentsAttribute()
    {
        return $this->getLastComments(3, true);
    }

    public function getProductOverview()
    {
        return $this->setAppends([
            'voteAverage',
            'voteCount',
            'commentCount',
            'recentComments',
        ]);
    }

    public function buyers()
    {
        return $this->belongsToMany(User::class, 'factors', 'product_id', 'user_id')
            ->withPivot('status')->withTimestamps();
    }
}
