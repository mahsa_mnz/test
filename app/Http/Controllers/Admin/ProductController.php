<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Mahsa\VoteComment\Models\VoteComment;

class ProductController extends Controller
{
    public function voteList(Product $product,Request $request)
    {
        $status = $request->get('status');
        $per_page = $request->get('per_page');
        $page = $request->get('page');

        return response()->json($product->getListOfVoteAndComments($status,$per_page,$page), Response::HTTP_OK);
    }

    public function reviewVote(Product $product, VoteComment $voteComment, Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $voteCommentable = $voteComment->voteCommentable;
        if ($voteCommentable != $product)
            return response()->json([
                'error' => 'The vote and comment not belong to given product'
            ], Response::HTTP_FORBIDDEN);

        $validator = Validator::make($request->all(), [
            'vote_status' => 'required|boolean',
            'comment_status' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $voteComment->review($user, $request->get('vote_status'),$request->get('comment_status'));

        return response()->json(null, Response::HTTP_OK);
    }
}
