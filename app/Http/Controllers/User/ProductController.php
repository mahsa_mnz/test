<?php


namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private ProductRepositoryInterface $productRepository;

    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request){

        $res = $this->paginate($this->productRepository->getListOfProducts(), $request);

        return response()->json($res, Response::HTTP_OK);
    }

    public function show(Product $product){

        return response()->json($product->getProductOverview(), Response::HTTP_OK);
    }

    public function vote(Product $product, Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($product->checkUserHasVote($user))
            return response()->json([
                'error' => 'Your vote has been registered on this product before'
            ], Response::HTTP_CONFLICT);

        switch ($product->rate_level){
            case 'no_one':
                return response()->json([
                    'error' => 'This Product is not ratable'
                ], Response::HTTP_BAD_REQUEST);
                break;
            case 'buyer':
                if (!$user->checkUserPurchased($product))
                    return response()->json([
                        'error' => 'Only users who bought this product can vote!'
                    ], Response::HTTP_BAD_REQUEST);
                break;
        }

        $validator = Validator::make($request->all(), [
            'vote' => 'required|integer|between:0,5'
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $voteComment = $product->vote($request->get('vote'), $user);
        return response()->json($voteComment, Response::HTTP_CREATED);
    }

    public function comment(Product $product, Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($product->checkUserHasComment($user))
            return response()->json([
                'error' => 'Your comment has been registered on this product before'
            ], Response::HTTP_CONFLICT);

        switch ($product->comment_level){
            case 'no_one':
                return response()->json([
                    'error' => 'This Product is not commentable'
                ], Response::HTTP_BAD_REQUEST);
                break;
            case 'buyer':
                if (!$user->checkUserPurchased($product))
                    return response()->json([
                        'error' => 'Only users who bought this product can vote!'
                    ], Response::HTTP_BAD_REQUEST);
                break;
        }

        $validator = Validator::make($request->all(), [
            'comment' => 'required|max:256'
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $voteComment = $product->comment($request->get('comment'), $user);
        return response()->json($voteComment, Response::HTTP_CREATED);
    }
}
