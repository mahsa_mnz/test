<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function paginate($query, Request $request)
    {
        $perPage = $request->has('per_page') ? $request->get('per_page') : 25;

        return $query->paginate($perPage)->withQueryString();
    }

    public function validationErrorResponse($validator)
    {
        $response['errors'] = $validator->messages();
        return response()->json($response, Response::HTTP_BAD_REQUEST);
    }
}
