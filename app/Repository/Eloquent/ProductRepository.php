<?php


namespace App\Repository\Eloquent;


use App\Models\Product;
use App\Models\User;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    public function getListOfProducts()
    {
        return DB::table('products')
            ->where('shown', '=', true)
            ->orderBy('created_at', 'DESC')
            ->select('products.id', 'products.title', 'products.description', 'products.rate_level', 'products.comment_level', 'products.created_at');
    }

    public function getProductOverview(Product $product)
    {
        return DB::table('products')
            ->where('products.shown', '=', true)
            ->select('products.id', 'products.title', 'products.description', 'products.rate_level', 'products.comment_level', 'products.created_at')
            ->addSelect([
                'voteAverage' => function ($query) use ($product) {
                    $query->selectRaw('AVG(vote) as voteAverage')
                        ->from('vote_comments')
                        ->where('vote_commentable_type', '=', $product->getMorphClass())
                        ->where('vote_commentable_id', '=', $product->id)
                        ->where('vote_status', '=', true)
                        ->whereNotNull('vote')
                        ->groupBy(['vote_commentable_type', 'vote_commentable_id']);
            },
                'voteCount' => function ($query) use ($product) {
                    $query->selectRaw('COUNT(vote) as voteCount')
                        ->from('vote_comments')
                        ->where('vote_commentable_type', '=', $product->getMorphClass())
                        ->where('vote_commentable_id', '=', $product->id)
                        ->where('vote_status', '=', true)
                        ->whereNotNull('vote')
                        ->groupBy(['vote_commentable_type', 'vote_commentable_id']);
            },
                'commentCount' => function ($query) use ($product) {
                    $query->selectRaw('COUNT(comment) as commentCount')
                        ->from('vote_comments')
                        ->where('vote_commentable_type', '=', $product->getMorphClass())
                        ->where('vote_commentable_id', '=', $product->id)
                        ->where('comment_status', '=', true)
                        ->whereNotNull('comment')
                        ->groupBy(['vote_commentable_type', 'vote_commentable_id']);
            }])
            ->find($product->id);
    }
}
