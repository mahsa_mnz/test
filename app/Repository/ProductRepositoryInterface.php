<?php


namespace App\Repository;


use App\Models\Product;

interface ProductRepositoryInterface
{
    public function getListOfProducts();

    public function getProductOverview(Product $product);
}
