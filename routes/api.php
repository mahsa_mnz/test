<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/login", function (Request $request){
    return response()->json([
        'errors' => ['Please Login!'],
    ], Response::HTTP_UNAUTHORIZED);
})->name('login');

Route::post("/login", function (Request $request){

    $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);
    if ($validator->fails()) {
        $response['errors'] = $validator->messages();
        return response()->json($response, Response::HTTP_BAD_REQUEST);
    }

    $user = \App\Models\User::where('email', $request->email)->first();

    if (! $user || ! \Illuminate\Support\Facades\Hash::check($request->password, $user->password)) {
        return response()->json([
            'email' => ['The provided credentials are incorrect.'],
        ], Response::HTTP_BAD_REQUEST);
    }
    return $user->createToken($request->device_name)->plainTextToken;
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('user')->middleware('role:user')->group(function () {
        Route::prefix('products')->group(function () {
            Route::get('/', [\App\Http\Controllers\User\ProductController::class, 'index']);
            Route::get('/{product}', [\App\Http\Controllers\User\ProductController::class, 'show']);
            Route::post('/{product}/vote', [\App\Http\Controllers\User\ProductController::class, 'vote']);
            Route::post('/{product}/comment', [\App\Http\Controllers\User\ProductController::class, 'comment']);
        });
    });
    Route::prefix('admin')->middleware('role:admin')->group(function () {
        Route::prefix('products')->group(function () {
            Route::get('/{product}/votes', [\App\Http\Controllers\Admin\ProductController::class, 'voteList']);
            Route::post('/{product}/votes/{voteComment}/review', [\App\Http\Controllers\Admin\ProductController::class, 'reviewVote']);
        });
    });
});
