<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->company,
            'description' => $this->faker->text,
            'owner_id' => User::factory()->state(['role' => 'admin']),
            'rate_level' => $this->faker->randomElement(['no_one', 'buyer', 'public']),
            'comment_level' => $this->faker->randomElement(['no_one', 'buyer', 'public']),
        ];
    }
}
