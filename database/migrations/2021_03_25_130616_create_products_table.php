<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title', 256);
            $table->text('description');
            $table->bigInteger('owner_id')->unsigned();
            $table->boolean('shown')->default(true);
            $table->enum('rate_level',['no_one', 'buyer', 'public'])->default('buyer');
            $table->enum('comment_level',['no_one', 'buyer', 'public'])->default('buyer');

            $table->foreign('owner_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
