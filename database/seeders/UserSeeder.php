<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->state(['role' => 'admin'])
            ->has(Product::factory()->hasVoteComments(20), 'products')
            ->count(10)
            ->create();

        // These two rows added temporarily for api route collection works fine with predefined data
        User::query()->where('role', '=', 'admin')->first()->update(['email' => 'admin@test.com']);
        User::query()->where('role', '=', 'user')->first()->update(['email' => 'user@test.com']);
    }
}
