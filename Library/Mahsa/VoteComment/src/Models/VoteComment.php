<?php

namespace Mahsa\VoteComment\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mahsa\VoteComment\database\factories\VoteCommentFactory;

class VoteComment extends Model
{
    use HasFactory;
    protected static function newFactory()
    {
        return VoteCommentFactory::new();
    }

    public $timestamps = true;

    public $fillable = ['user_id','vote','comment','vote_status','comment_status','reviewer_id','reviewed_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reviewer()
    {
        return $this->belongsTo(User::class);
    }

    public function voteCommentable()
    {
        return $this->morphTo();
    }

    public function review(User $reviewer, $voteStatus, $commentStatus){
        $this->update([
            'reviewer_id' => $reviewer->id,
            'vote_status' => $voteStatus,
            'comment_status' => $commentStatus,
            'reviewed_at' => Carbon::now()
        ]);
    }
}
