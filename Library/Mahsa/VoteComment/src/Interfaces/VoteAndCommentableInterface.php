<?php


namespace Mahsa\VoteComment\Interfaces;


use App\Models\User;
use phpDocumentor\Reflection\Types\Boolean;

interface VoteAndCommentableInterface
{
    /**
     * This function will get the last comments on the product
     * @param $count //number of comments which should be returned
     * @param $isConfirmed //return just confirmed comments if this param is true
     * @return mixed
     */
    public function getLastComments($count, $isConfirmed);

    /**
     * This function will return number of votes for the product
     * @param $isConfirmed //count just confirmed comments if this param is true
     * @return int
     */
    public function getVoteCount($isConfirmed);

    /**
     * This function will return number of comments for the product
     * @param $isConfirmed //count just confirmed comments if this param is true
     * @return int
     */
    public function getCommentCount($isConfirmed);

    /**
     * This function will calculate average of confirmed votes
     * @return mixed
     */
    public function getVoteAverage();

    /**
     * voter can vote for the product via this function
     * @param $vote
     * @param User $voter
     * @return mixed
     */
    public function vote($vote, User $voter);

    /**
     * voter can add comment for the product via this function
     * @param $comment
     * @param User $voter
     * @return mixed
     */
    public function comment($comment, User $voter);

    /**
     * This function will return paginated votes and comments for the product via given status
     * @param $status
     * @param $perPage
     * @param $page
     * @return mixed
     */
    public function getListOfVoteAndComments($status, $perPage, $page);

    /**
     * This function check if user has voted the product
     * @param User $user
     * @return boolean
     */
    public function checkUserHasVote(User $user);

    /**
     * This function check if user has added comment on the product
     * @param User $user
     * @return boolean
     */
    public function checkUserHasComment(User $user);
}
