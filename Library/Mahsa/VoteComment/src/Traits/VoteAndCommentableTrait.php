<?php


namespace Mahsa\VoteComment\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Mahsa\VoteComment\Models\VoteComment;

trait VoteAndCommentableTrait
{
    /**
     * @return MorphMany
     */
    public function voteComments()
    {
        return $this->morphMany(VoteComment::class, 'vote_commentable');
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeVoteConfirmed($query)
    {
        return $query->where('vote_status', '=', true);
    }

    public function scopeCommentConfirmed($query)
    {
        return $query->where('comment_status', '=', true);
    }

    public function getLastComments($count = 3, $isConfirmed = true)
    {
        $query = $this->voteComments();
        if ($isConfirmed)
            $query->where('comment_status', '=', true);
        return $query->orderBy('created_at', 'desc')
            ->limit($count)
            ->select('user_id','vote','comment')
            ->with(['user' => function($q){
                $q->select("id","name", "email");
            }])
            ->get();
    }

    public function getVoteCount($isConfirmed)
    {
        $query = $this->voteComments();
        if ($isConfirmed)
            $query->where('vote_status', '=', true);
        return $query->count();
    }

    public function getCommentCount($isConfirmed)
    {
        $query = $this->voteComments()->whereNotNull('comment');
        if ($isConfirmed)
            $query->where('comment_status', '=', true);
        return $query->count();
    }

    public function getVoteAverage()
    {
        return $this->voteComments()
            ->where('vote_status', '=', true)
            ->avg('vote');
    }

    public function vote($vote, User $voter)
    {
        $voteComment = $this->voteComments()->where('user_id', $voter->id)->first();
        if (!$voteComment)
            $voteComment = $this->voteComments()
                ->create([
                    'user_id' => $voter->id,
                    'vote' => $vote,
                ]);
        else
            $voteComment->update([
                'vote' => $vote
            ]);

        return $voteComment;
    }

    public function comment($comment, User $voter)
    {
        $voteComment = $this->voteComments()->where('user_id', $voter->id)->first();
        if (!$voteComment)
            $voteComment = $this->voteComments()
                ->create([
                    'user_id' => $voter->id,
                    'comment' => $comment
                ]);
        else
            $voteComment->update([
                'comment' => $comment
            ]);

        return $voteComment;
    }

    public function getListOfVoteAndComments($status = null, $perPage = null, $page = null)
    {
        $query = $this->voteComments();
        switch ($status)
        {
            case 'not-reviewed-vote':
                $query->where('vote_status', null);
                break;
            case 'confirmed-vote':
                $query->where('vote_status', true);
                break;
            case 'rejected-vote':
                $query->where('vote_status', false);
                break;
            case 'not-reviewed-comment':
                $query->where('comment_status', null);
                break;
            case 'confirmed-comment':
                $query->where('comment_status', true);
                break;
            case 'rejected-comment':
                $query->where('comment_status', false);
                break;
        }
        return $query->orderBy('created_at', 'DESC')
            ->paginate($perPage, ['*'], 'Vote and comments list', $page);
    }

    public function checkUserHasVote(User $user)
    {
        return $this->voteComments()->where('user_id', $user->id)->whereNotNull('vote')->exists();
    }

    public function checkUserHasComment(User $user)
    {
        return $this->voteComments()->where('user_id', $user->id)->whereNotNull('comment')->exists();
    }
}
