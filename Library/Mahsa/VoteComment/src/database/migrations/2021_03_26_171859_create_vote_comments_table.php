<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoteCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_comments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->morphs('vote_commentable');
            $table->integer('vote')->nullable(true);
            $table->text('comment')->nullable(true);
            $table->boolean('vote_status')->nullable(true);
            $table->boolean('comment_status')->nullable(true);
            $table->timestamps();
            $table->bigInteger('reviewer_id')->unsigned()->nullable(true);
            $table->timestamp('reviewed_at')->nullable(true);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reviewer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote_comments');
    }
}
