<?php

namespace Mahsa\VoteComment\database\factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Mahsa\VoteComment\Models\VoteComment;

class VoteCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VoteComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->state(['role' => 'user']),
            'vote' => $this->faker->biasedNumberBetween(0,5),
            'comment' => $this->faker->text(256),
            'vote_status' => $this->faker->boolean(80),
            'comment_status' => $this->faker->boolean(60),
        ];
    }
}
